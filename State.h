#ifndef INCLUDED_STATE_H
#define INCLUDED_STATE_H

#include "Array2D.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <gl/glew.h>
#include <GLFW/glfw3.h>
#include "Image.h"
// glmの使う機能をインクルード
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using glm::vec3;
using glm::vec4;
using glm::mat4;

class Image; //宣言だけしておけばいい。インクルードしなくていい。

			 //状態クラス
class State {
public:
	
	State(const char* stageData, int size);
	~State();

	
	void setSize(const char* stageData, int size);
	void draw(const mat4 &projectionMat, const mat4 &viewMat)const;
	void update(int moveX, int moveY);
	
	//void drawcell(int x, int y, int id, const mat4 &projectionMat, const mat4 &viewMat);

	//void drawback(const mat4 &projectionMat,const mat4 &viewMat);
	//void drawfront(const mat4 &projectionMat, const mat4 &viewMat);
	//void drawbox(int x,int y,const mat4 &projectionMat, const mat4 &viewMat);
	//void drawplayer(int x,int y,const mat4 &projectionMat, const mat4 &viewMat);

	bool hasCleared() const;

private:

	Image* mImage;

	class Object;

	int mWidth;
	int mHeight;
	Array2D< Object > mObjects;
	
	int mMoveCount;

	

};
#endif