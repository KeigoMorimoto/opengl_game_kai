﻿#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <gl/glew.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <windows.h>
#include "Fps.h"
#include "State.h"
#include "Image.h"
#include "File.h"

// glmの使う機能をインクルード
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
//using namespace glm;でもいいけどここでは一部のみ「glm::」を省力できるようにする
using glm::vec3;
using glm::vec4;
using glm::mat4;

GLFWwindow* initGLFW(int width, int height)
{
	// GLFW初期化
	if (glfwInit() == GL_FALSE)
	{
		return nullptr;
	}
	// ウィンドウ生成
	GLFWwindow* window = glfwCreateWindow(width, height, "OpenGL Sample", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return nullptr;
	}
	// バージョン2.1指定
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(0);
	// GLEW初期化
	if (glewInit() != GLEW_OK)
	{
		return nullptr;
	}
	return window;
}

int main()
{
	Fps fps;
	GLint width = 800, height = 600;
	GLFWwindow* window = initGLFW(width, height);

	// タイマーのセッティング
	double FPS = 60.0; // <--- 一秒間に更新する回数(30 か 60) 
	double currentTime, lastTime, elapsedTime;
	currentTime = lastTime = elapsedTime = 0.0;
	glfwSetTime(0.0); // <--- タイマーを初期化する

					  /* 光源の初期設定 */
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	GLfloat light0pos[] = { 0.0, 3.0, 5.0, 1.0 };

	//glEnable(GL_CULL_FACE);

	double count = 0;

	// 頂点データ
	float vertex_position[] = {
		5.0f, 5.0f,-5.0f,
		-5.0f, 5.0f,-5.0f,
		-5.0f, 5.0f,5.0f,
		5.0f, 5.0f,5.0f
	};

	const GLfloat vertex_uv[] = {
		1, 0,
		0, 0,
		0, 1,
		1, 1,
	};

	// 頂点データ
	float vertex_position2[] = {
		0.5f, 0.0f,-0.5f,
		-0.5f, 0.0f,-0.5f,
		0.0f, 0.0f,0.5f,
		1.5f, 0.5f,1.5f
	};

	const GLfloat vertex_color[] = {
		1.0f,0.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,1.0f,
		0.0f,0.0f,0.0f,1.0f
	};

	//GLuint texID = loadTexture("stage1.raw");

	//State* mState = new State("stageData.txt",20);
	Image image;

	File file("stageData.txt");

	if (!(file.data())) { //データない！
		std::cout << "stage file could not be read." << std::endl;
	}

	State *state = new State(file.data(), file.size());
	
	mat4 viewMat, projectionMat, inverseMat;

	int dx, dy = 0;
	// フレームループ
	while (glfwWindowShouldClose(window) == GL_FALSE)
	{
		currentTime = glfwGetTime();
		elapsedTime = currentTime - lastTime;

		//fps.Updata();
		//fps.Wait();

		if (elapsedTime >= 1.0 / FPS) {

			count ++;

			//glUseProgram(shader);
			glEnable(GL_DEPTH_TEST);
			//glDepthFunc(GL_LESS);
			glClearColor(0.2f, 0.2f, 0.2f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			// 宣言時には単位行列が入っている
			

			// View行列を計算
			viewMat = glm::lookAt(
				vec3(0.0, 400.0,200.0), // ワールド空間でのカメラの座標
				vec3(0.0, 0.0, 0.0), // 見ている位置の座標
				vec3(0.0, 1.0, 0.0)  // 上方向を示す。(0,1.0,0)に設定するとy軸が上になります
			);

			// Projection行列を計算
			projectionMat = glm::perspective(
				glm::radians(45.0f), // ズームの度合い(通常90〜30)
				(GLfloat)width / (GLfloat)height,		// アスペクト比
				0.1f,		// 近くのクリッピング平面
				1000.0f		// 遠くのクリッピング平面
			);


			//state.drawcell(0,0,0,projectionMat, viewMat);
			//state.drawfront(projectionMat,viewMat);
			//state.drawbox(0,0,projectionMat, viewMat);
			//image.draw(1, 0, 0, projectionMat, viewMat);
			//image.draw(2, 0, 1,projectionMat,viewMat);
			//image.draw(3, 0, 2, projectionMat, viewMat);
			//image.draw(4, 0, 3, projectionMat, viewMat);
			//image.draw(1.5, 0, 0, projectionMat, viewMat);
			dx = 0;
			dy = 0;


			if (glfwGetKey(window, GLFW_KEY_W) != GLFW_RELEASE) {
				std::cout << "W\n";
				dy = -1;
			}

			if (glfwGetKey(window, GLFW_KEY_S) != GLFW_RELEASE) {
				std::cout << "S\n";
				dy = 1;
			}

			if (glfwGetKey(window, GLFW_KEY_A) != GLFW_RELEASE) {
				std::cout << "A\n";
				dx = -1;
			}

			if (glfwGetKey(window, GLFW_KEY_D) != GLFW_RELEASE) {
				std::cout << "D\n";
				dx = 1;
			}

			state->update(dx,dy);
			state->draw(projectionMat, viewMat);
			
			lastTime = glfwGetTime();

			// ダブルバッファのスワップ
			glfwSwapBuffers(window);
			glfwPollEvents();
		}
		//Sleep();

	}
	// GLFWの終了処理
	glfwTerminate();
	return 0;
}