#include "Image.h"


int readShaderSource(GLuint shaderObj, std::string fileName)
{
	//ファイルの読み込み
	std::ifstream ifs(fileName);
	if (!ifs)
	{
		std::cout << "error" << std::endl;
		return -1;
	}
	std::string source;
	std::string line;
	while (getline(ifs, line))
	{
		source += line + "\n";
	}
	// シェーダのソースプログラムをシェーダオブジェクトへ読み込む
	const GLchar *sourcePtr = (const GLchar *)source.c_str();
	GLint length = source.length();
	glShaderSource(shaderObj, 1, &sourcePtr, &length);
	return 0;
}
GLint makeShader(std::string vertexFileName, std::string fragmentFileName)
{
	// シェーダーオブジェクト作成
	GLuint vertShaderObj = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragShaderObj = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint shader;
	// シェーダーコンパイルとリンクの結果用変数
	GLint compiled, linked;
	/* シェーダーのソースプログラムの読み込み */
	if (readShaderSource(vertShaderObj, vertexFileName)) return -1;
	if (readShaderSource(fragShaderObj, fragmentFileName)) return -1;
	/* バーテックスシェーダーのソースプログラムのコンパイル */
	glCompileShader(vertShaderObj);
	glGetShaderiv(vertShaderObj, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		fprintf(stderr, "Compile error in vertex shader.\n");
		return -1;
	}
	/* フラグメントシェーダーのソースプログラムのコンパイル */
	glCompileShader(fragShaderObj);
	glGetShaderiv(fragShaderObj, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		fprintf(stderr, "Compile error in fragment shader.\n");
		return -1;
	}
	/* プログラムオブジェクトの作成 */
	shader = glCreateProgram();
	/* シェーダーオブジェクトのシェーダープログラムへの登録 */
	glAttachShader(shader, vertShaderObj);
	glAttachShader(shader, fragShaderObj);
	/* シェーダーオブジェクトの削除 */
	glDeleteShader(vertShaderObj);
	glDeleteShader(fragShaderObj);
	/* シェーダープログラムのリンク */
	glLinkProgram(shader);
	glGetProgramiv(shader, GL_LINK_STATUS, &linked);
	if (linked == GL_FALSE)
	{
		fprintf(stderr, "Link error.\n");
		return -1;
	}
	return shader;
}
GLuint loadTexture(std::string filename)
{
	// テクスチャIDの生成
	GLuint texID;
	glGenTextures(1, &texID);

	// ファイルの読み込み
	std::ifstream fstr(filename, std::ios::binary);
	const size_t fileSize = static_cast<size_t>(fstr.seekg(0, fstr.end).tellg());
	fstr.seekg(0, fstr.beg);
	char* textureBuffer = new char[fileSize];
	fstr.read(textureBuffer, fileSize);

	// テクスチャをGPUに転送
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 256, 256, 0, GL_RGB, GL_UNSIGNED_BYTE, textureBuffer);

	// テクスチャの設定
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// テクスチャのアンバインド
	delete[] textureBuffer;
	glBindTexture(GL_TEXTURE_2D, 0);

	return texID;
}

void drawTexture(GLuint textureLocation, GLuint positionLocation, GLuint uvLocation, GLuint texID, float *vertex_position, const GLfloat *vertex_uv) {

	// uniform属性の登録(おまじない)
	//glUniform1i(textureLocation, 0);

	// attribute属性を登録
	glVertexAttribPointer(positionLocation, 3, GL_FLOAT, false, 0, vertex_position);
	glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 0, vertex_uv);

	// モデルの描画

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
};


Image::Image() {
	shader = makeShader("texture.vert", "texture.frag");
	simple_shader = makeShader("simple.vert", "simple.frag");

	matrixID = glGetUniformLocation(shader, "MVP");
	simple_matrixID = glGetUniformLocation(simple_shader, "MVP");

	positionLocation = glGetAttribLocation(shader, "position");
	uvLocation = glGetAttribLocation(shader, "uv");
	textureLocation = glGetUniformLocation(shader, "texture");

	simple_positionLocation = glGetAttribLocation(simple_shader, "position");
	simple_colorLocation = glGetAttribLocation(simple_shader, "color");

	glEnableVertexAttribArray(positionLocation);
	glEnableVertexAttribArray(uvLocation);
	glEnableVertexAttribArray(textureLocation);
	
	glEnableVertexAttribArray(simple_positionLocation);
	glEnableVertexAttribArray(simple_colorLocation);


	texID[0] = loadTexture("stage1.raw");
	texID[1] = loadTexture("goal.raw");
	

}

Image::~Image() {
}

void Image::draw(int X, int Y, int ID, const mat4 &projectionMat, const mat4 &viewMat) {
	if (ID == 0) {
		glUseProgram(simple_shader);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(simple_matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glVertexAttribPointer(simple_positionLocation, 3, GL_FLOAT, false, 0, player_vertex_position);
		glVertexAttribPointer(simple_colorLocation, 4, GL_FLOAT, false, 0, player_color);

		glDrawElements(GL_TRIANGLES, 18, GL_UNSIGNED_INT, player_index);

		glUseProgram(0);
	}
	if (ID == 1) {
		glUseProgram(simple_shader);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(simple_matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glVertexAttribPointer(simple_positionLocation, 3, GL_FLOAT, false, 0, wall_vertex_position);
		glVertexAttribPointer(simple_colorLocation, 4, GL_FLOAT, false, 0, wall_color);

		glDrawElements(GL_TRIANGLES, 18, GL_UNSIGNED_INT, wall_index);

		glUseProgram(0);
	}

	if (ID == 2) {
		glUseProgram(simple_shader);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(simple_matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		glVertexAttribPointer(simple_positionLocation, 3, GL_FLOAT, false, 0, block_vertex_position);
		glVertexAttribPointer(simple_colorLocation, 4, GL_FLOAT, false, 0, block_color);

		glDrawElements(GL_TRIANGLES, 18, GL_UNSIGNED_INT, block_index);

		glUseProgram(0);
	}

	if (ID == 3) {
		glUseProgram(shader);
		
		glBindTexture(GL_TEXTURE_2D, texID[1]);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		drawTexture(textureLocation, positionLocation, uvLocation, texID[1], vertex_position, vertex_uv);

		glUseProgram(0);
	}

	if (ID == 4) {
		glUseProgram(shader);

		glBindTexture(GL_TEXTURE_2D, texID[0]);

		modelMat = glm::translate(glm::mat4(1.0f), vec3(X, 0, Y));
		mvpMat = projectionMat * viewMat * modelMat;

		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mvpMat[0][0]);

		drawTexture(textureLocation, positionLocation, uvLocation, texID[0], vertex_position, vertex_uv);

		glUseProgram(0);
	}

		
};


