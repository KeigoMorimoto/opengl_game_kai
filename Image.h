#ifndef INCLUDED_IMAGE_H
#define INCLUDED_IMAGE_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <gl/glew.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <windows.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using glm::vec3;
using glm::vec4;
using glm::mat4;

class Image {
public:
	Image();
	~Image();

	void draw(int X,int Y,int ID, const mat4 &projectionMat, const mat4 &viewMat);

private:

	GLint simple_shader;
	GLint shader;

	GLuint texID[2];
	GLuint matrixID;
	GLuint simple_matrixID;

	int positionLocation;
	int simple_positionLocation;
	int simple_colorLocation;
	int uvLocation;
	int textureLocation;


	// ���_�f�[�^
	float vertex_position[12] = {
		16.0f, 0.0f,-16.0f,
		-16.0f, 0.0f,-16.0f,
		-16.0f, 0.0f,16.0f,
		16.0f, 0.0f,16.0f
	};

	const GLfloat vertex_uv[8] = {
		1, 0,
		0, 0,
		0, 1,
		1, 1,
	};

	GLfloat player_vertex_position[15] = {
		0.0f,32.0f,0.0f,
		16.0f, 0.0f,-16.0f,
		-16.0f, 0.0f,-16.0f,
		-16.0f, 0.0f,16.0f,
		16.0f, 0.0f,16.0f
	};

	GLfloat player_color[20] = {
		0.3f, 0.3f, 0.3f,0.0f,
		0.0f, 0.0f, 0.0f,0.0f,
		0.0f, 0.0f, 0.0f,0.0f,
		0.0f, 0.0f, 0.0f,0.0f,
		0.0f, 0.0f, 0.0f,0.0f,
	};

	GLuint player_index[18] = {
		0,1,2,
		0,2,3,
		0,3,4,
		0,4,1,
		2,1,3,
		1,4,2
	};

	GLfloat block_vertex_position[15] = {
		0.0f,32.0f,0.0f,
		16.0f, 0.0f,-16.0f,
		-16.0f, 0.0f,-16.0f,
		-16.0f, 0.0f,16.0f,
		16.0f, 0.0f,16.0f
	};

	GLfloat block_color[20] = {
		1.0f, 1.0f, 1.0f,0.0f,
		0.3f, 0.3f, 0.3f,0.0f,
		0.3f, 0.3f, 0.3f,0.0f,
		0.3f, 0.3f, 0.3f,0.0f,
		0.3f, 0.3f, 0.3f,0.0f,
	};

	GLuint block_index[18] = {
		0,1,2,
		0,2,3,
		0,3,4,
		0,4,1,
		2,1,3,
		1,4,2
	};

	GLfloat wall_vertex_position[15] = {
		0.0f,16.0f,0.0f,
		16.0f, 0.0f,-16.0f,
		-16.0f, 0.0f,-16.0f,
		-16.0f, 0.0f,16.0f,
		16.0f, 0.0f,16.0f
	};

	GLfloat wall_color[20] = {
		1.0f, 1.0f, 1.0f,0.0f,
		1.0f, 0.3f, 0.3f,0.0f,
		1.0f, 0.3f, 0.3f,0.0f,
		1.0f, 0.3f, 0.3f,0.0f,
		1.0f, 0.3f, 0.3f,0.0f,
	};

	GLuint wall_index[18] = {
		0,1,2,
		0,2,3,
		0,3,4,
		0,4,1,
		2,1,3,
		1,4,2
	};


	mat4 modelMat;
	mat4 mvpMat;

};
#endif